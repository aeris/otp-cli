require 'tempfile'

describe OtpCli::Config do
	around(:each) do |example|
		Tempfile.open 'otp' do |file|
			ENV['OTP_CONFIG'] = file.path
			@otp              = OtpCli::Config.new
			example.run
		end
	end

	describe '#add' do
		it 'must add OTP from string' do
			otp = @otp.add 'otpauth://totp/test?secret=TEST'
			expect(otp).not_to be_nil
		end

		it 'must refuse invalid OTP from string' do
			expect { @otp.add 'otpauth://totp/test' }
					.to raise_error 'Invalid OTP secret otpauth://totp/test'
		end
	end

	describe '#add_qrcode' do
		it 'must add OTP from QR code' do
			Tempfile.open 'qrcode' do |qr|
				img = RQRCode::QRCode.new 'otpauth://totp/test?secret=TEST'
				IO.write qr, img.as_png
				otp = @otp.add_qrcode qr.path
				expect(otp).not_to be_nil
			end
		end

		it 'must refuse invalid OTP from string' do
			expect do
				Tempfile.open 'qrcode' do |qr|
					img = RQRCode::QRCode.new 'otpauth://totp/test'
					IO.write qr, img.as_png
					otp = @otp.add_qrcode qr.path
					expect(otp).not_to be_nil
				end
			end.to raise_error 'Invalid OTP secret otpauth://totp/test'
		end
	end

	describe '#select' do
		it 'must return the OTP if only one match' do
			@otp.add 'otpauth://totp/test1?secret=TEST'
			@otp.add 'otpauth://totp/test2?secret=TEST'

			otp = @otp.select 'test1'
			expect(otp.name).to eq 'test1'
			otp = @otp.select 'test2'
			expect(otp.name).to eq 'test2'
		end

		it 'must ask the OTP if only one match' do
			@otp.add 'otpauth://totp/test1?secret=TEST'
			@otp.add 'otpauth://totp/test2?secret=TEST'

			allow(STDIN).to receive(:gets).and_return '1', '2'

			otp = @otp.select 'test'
			expect(otp.name).to eq 'test1'
			otp = @otp.select 'test'
			expect(otp.name).to eq 'test2'
		end

		it 'must return no OTP if nothing match' do
			@otp.add 'otpauth://totp/test1?secret=TEST'
			@otp.add 'otpauth://totp/test2?secret=TEST'

			expect { @otp.select 'foo' }.to raise_error 'No such OTP'
		end
	end
end
