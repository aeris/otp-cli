lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'otp-cli'

Gem::Specification.new do |spec|
	spec.name    = 'otp-cli'
	spec.version = OtpCli::VERSION
	spec.authors = ['aeris']
	spec.email   = ['aeris@imirhil.fr']
	spec.license = 'AGPL-3.0+'

	spec.summary  = 'TOTP/HOTP CLI'
	spec.homepage = 'https://git.imirhil.fr/aeris/otp-cli'

	spec.files         = `git ls-files -z`.split("\x0").reject do |f|
		f.match(%r{^(test|spec|features)/})
	end
	spec.bindir        = 'bin'
	spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
	spec.require_paths = ['lib']

	spec.add_development_dependency 'bundler', '~> 1.15'
	spec.add_development_dependency 'rspec', '~> 3.6'

	spec.add_dependency 'rotp', '~> 3.3'
	spec.add_dependency 'rqrcode', '~> 0.10'
	spec.add_dependency 'clipboard', '~> 1.1'
end
