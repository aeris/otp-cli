# OTP CLI

## What is this ?

Simple TOTP/HOTP command line

## How to install ?

To install it, simply use `gem install otp-cli`

This tool requires `zbar-tools` for QR code import and `imagemagick` for QR code display (currently no simple way to do this with pure Ruby)

## How to use it ?

	Usage: otp [options] <filter>
		-a, --add STRING	Add secret from string
		-i, --img PATH		Add secret from QR code
		-q, --qrcode NAME	Display qrcode

To add a secret, import from a string or from a QR Code

	otp -a otpauth://totp/name?secret=ASECRET
	otp -i /tmp/qrcode.png

Secrets are saved onto `$HOME/.otp`. You can override location with `OTP_CONFIG` environment variable.

Supported OTP URI options are

	* name (mandatory)
	* secret (mandatory)
	* issuer
	* algorithm (default to SHA-1)
	* digits (default to 6)
	* period (default to 30s)

To issue a PIN

	otp OTP
	such OTP
	595905 (25s)

PIN is automatically copied into your clipboard

If multiple OTP match your filter, you will be asked for the one to use:

	otp OTP
	[1] such OTP
	[2] wow OTP
	[3] many OTP
	2
	wow OTP
	272784 (1s)

If you need to display a secret on a QR code, for example to import the OTP elsewhere

	otp -q OTP
