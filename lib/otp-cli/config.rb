require 'clipboard'
require 'open3'

module OtpCli
	class Config
		def initialize
			@file = ENV.fetch('OTP_CONFIG') { File.join Dir.home, '.otp' }
			FileUtils.touch @file unless File.exists? @file

			@otps = IO.readlines(@file).collect { |line| OTP.get line.chomp }
		end

		def add(secret)
			begin
				otp = OTP.get secret
			rescue
				raise "Invalid OTP secret #{secret}"
			end

			puts "Adding secret #{secret}"
			File.open(@file, 'a') do |f|
				f.puts secret
			end
			@otps << otp
			otp
		end

		def add_qrcode(path)
			data, = Open3.capture2 'zbarimg', '--raw', '-q', path
			self.add data.chomp
		end

		def otp(id)
			otp = @otps[id]
			raise 'No such OTP' unless otp
			otp
		end

		def select(filter)
			otps = if filter
					   @otps.select { |o| o.to_s.downcase.include? filter }
				   else
					   @otps
				   end

			case otps.size
			when 0
				raise 'No such OTP'
			when 1
				otp = otps.first
				puts otp
				otp
			else
				len = otps.size.to_s.size
				otps.each_with_index do |otp, n|
					puts "[#{(n + 1).to_s.rjust len}] #{otp}"
				end

				number = STDIN.gets.to_i - 1
				otps[number]
			end
		end
	end
end
