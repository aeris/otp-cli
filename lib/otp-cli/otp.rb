require 'cgi'
require 'rotp'
require 'tempfile'
require 'uri'
require 'rqrcode'

module OtpCli
	class OTP
		attr_reader :name, :issuer

		def self.get(line)
			uri       = URI line
			type      = uri.host.to_sym
			name      = uri.path.sub %r{^/}, ''
			query     = CGI.parse uri.query
			secret    = query['secret'].first
			issuer    = query['issuer'].first
			algorithm = query['algorithm'].first || 'sha1'
			digits    = numeric_value query, 'digits', 6
			counter   = numeric_value query, 'counter'
			period    = numeric_value query, 'period', 30

			case type
			when :hotp then
				HOTP.new line, name, secret, issuer, algorithm, digits, counter
			when :totp then
				TOTP.new line, name, secret, issuer, algorithm, digits, period
			end
		end

		def qrcode
			::Tempfile.open %w[qrcode .png] do |file|
				qrcode = RQRCode::QRCode.new @line
				IO.write file, qrcode.as_png
				yield file
			end
		end

		def to_s
			s = self.name.clone
			s << %{ (#{self.issuer})} unless self.issuer.nil?
			s
		end

		def <=>(other)
			self.to_s.downcase <=> other.to_s.downcase
		end

		protected

		def initialize(otp, line, name, secret, issuer=nil, algorithm='sha1', digits=6)
			@otp       = otp
			@line      = line
			@name      = name
			@secret    = secret
			@issuer    = issuer
			@algorithm = algorithm
			@digits    = digits
		end

		private_class_method

		def self.numeric_value(query, name, default_value = nil)
			value = query[name].first
			return default_value if value.nil?
			value.to_i
		end
	end

	class HOTP < OTP
		def initialize(line, name, secret, issuer=nil, algorithm='sha1', digits=6, counter=0)
			super line, name, secret, issuer, algorithm, digits
			@counter = counter
		end
	end

	class TOTP < OTP
		def initialize(line, name, secret, issuer=nil, algorithm='sha1', digits=6, period=30)
			@period = period
			otp     = ::ROTP::TOTP.new secret, digits: digits, digest: algorithm, interval: period
			super otp, line, name, secret, issuer, algorithm, digits
		end

		def code
			@otp.now
		end

		def delay
			@period - (Time.now.to_i % @period)
		end
	end
end
